local optim = require('optim')
local func = require('pl.func')


local function retype(tensor)
	return tensor:type(torch.getdefaulttensortype())
end

local function addSingleton(tensor)
	return tensor:resize(1, unpack(tensor:size():totable()))
end

local function variableLength(action, input, target)
	local loss = 0
	local times = #input
	for i=1,times do
		local inputPart = addSingleton(input[i])
		local targetPart = addSingleton(input[i])
		loss = loss + action(inputPart, targetPart)
	end
	return loss
end

local function forward(body, head, criterion, input, target)
	-- input = retype(input)
	target = retype(target)
	local prediction = head:forward(body:forward(input))
	return criterion:forward(prediction, target)
end

local function forwardBack(body, head, criterion, input, target)
	-- input = retype(input)
	target = retype(target)
	-- Forward
	local bodyOutput = body:forward(input)
	local headOutput = head:forward(bodyOutput)
	-- print("Outputs:")
	-- print(headOutput[{{1,3}}])
	-- print("Targets:")
	-- print(target[{{1,3}}])


	local loss = criterion:forward(headOutput, target)

	-- Backward
	local gradient = criterion:backward(headOutput, target)
	-- print("Gradient:")
	-- print(gradient[{{1,3}}])

	gradient = head:backward(bodyOutput, gradient)
	gradient = body:backward(input, gradient)

	return loss
end

local function zero(tabl)
	for k,v in pairs(tabl) do
		v:zeroGradParameters()
	end
end

local function feval(x_new, x, dloss_dx)
	if (x ~= x_new) then
		x:copy(x_new)
	end
	return x, dloss_dx
end


return function (body, heads, criterion, opts)

	local config = {
		learningRate = opts.lr,
		learningRateDecay = opts.lrDecay,
		weightDecay = opts.weightDecay
	}

	local bodyState = {}


	local states = {}
	-- Weights and gradients
	-- model:backward() updates dloss_dx
	local x, dloss_dx = body:getParameters()
	local bodyFEval = func.bind(feval, func._1, x, dloss_dx)
	local bodyUpdate = func.bind(optim[opts.learningAlg], bodyFEval, x, bodyState)


	local trainingSteps = {}
	local valSteps = {}
	local updates = {}

	-- Bind functions for training (forward+back) step, validation (forward) step and update step
	for idx, head in pairs(heads) do

		-- The training step
		local boundForwardBack = func.bind(forwardBack, body, head, criterion, func._1, func._2)
		trainingSteps[idx] = boundForwardBack

		-- The validation step
		local boundForward = func.bind(forward, body, head, criterion, func._1, func._2)
		valSteps[idx] = boundForward

		if (opts.fixedSizeInput == 0) then
			trainingSteps[idx] = func.bind1(variableLength, boundForwardBack)
			valSteps[idx] = func.bind1(variableLength, boundForward)
		end

		-- Update step
		states[idx] = {}
		local headX, headDloss_dx = head:getParameters()
		local headFEval = func.bind(feval, func._1, headX, headDloss_dx)
		updates[idx] = func.bind(optim[opts.learningAlg], headFEval, headX, states[idx])

	end

	local input, target


	local function updateAfterAll(iters)

		-- Zero out gradients
		body:zeroGradParameters()
		zero(heads)

		-- For each head: forward+back a minibatch
		local loss = {}
		for idx,head in pairs(heads) do
			-- print("For head  ================   " .. idx .. "    ==================")
			local sample = iters[idx]()
			loss[idx] = trainingSteps[idx](sample.input, sample.target)
		end

		-- For body, and each head: update
		bodyUpdate()
		for idx,head in pairs(heads) do
			updates[idx]()
		end

		return loss
	end

	local function updateAfterEach(iters)

		-- Zero out gradients
		body:zeroGradParameters()
		zero(heads)

		-- For each head: forward+back+update a minibatch
		local loss = {}
		for idx,head in pairs(heads) do
			local sample = iters[idx]()
			loss[idx] = trainingSteps[idx](sample.input, sample.target)
			updates[idx]()
			bodyUpdate()
		end

		return loss

	end



	-- Return a function that iterates through training using a sample iterator
	return (opts.updateAfterEach and updateAfterEach or updateAfterAll),
	-- and a function that iterates through validation using a sample iterator
	function(iters)
		local loss = {}

		for idx,head in pairs(heads) do
			local sample = iters[idx]()
			loss[idx] = valSteps[idx](sample.input, sample.target)
		end

		return loss
	end
end
