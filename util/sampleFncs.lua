local fncs = {}

function fncs.variableSize (data, minSeq)
	local nPos = #(data.pos)
	local nNeg = #(data.neg)
	-- Returns a single example at once
	return function()
		local whichSet = torch.random(0,1)
		local review
		if (whichSet == 1) then
			local whichOne = torch.random(1,nPos)
			review = data.pos[whichOne]:cuda()
		else 
			local whichOne = torch.random(1,nNeg)
			review = data.neg[whichOne]:cuda()
		end
		-- If it's smaller than the smallest allowable size, pad with 0's
		if (review:size(1) < minSeq) then
			local tmp = torch.CudaTensor(minSeq, review:size(2)):zero()
			tmp[{{1, review:size(1)}}] = review
			review = tmp
		end
		return review, torch.Tensor{whichSet}:cuda()
	end
end

function fncs.fixedSize(data, atOnce)
	local randomSet = function (dataPosNeg, howMany)
		local size = dataPosNeg:size(1)
		local inds = torch.LongTensor(howMany)
		inds:apply(function() return torch.random(1, size) end)
		return dataPosNeg:index(1, inds):cuda()
	end
	-- Returns a minibatch at once
	return function()
		local half = math.floor(atOnce/2)

		local pos = randomSet(data.pos, half)
		local neg = randomSet(data.neg, atOnce-half)

		local reviews = pos:cat(neg, 1)
		local labels = torch.CudaTensor(atOnce):fill(0)
		labels[{{1, half}}] = 1

		-- Return the reviews and the labels
		return reviews, labels
	end
end

function fncs.makeSampleFnc(data, fixedSize)
	if (fixedSize == 0) then
		return fncs.variableSize(data, opts.minSeq)
	else
		return fncs.fixedSize(data, opts.minibatchSize)
	end
end

return fncs
