local path = require('pl.path')

local showoff = require('showoff')

local function loadFrame(notebook, id, name, dims)
    local frame
    if (id == nil) then
        frame = notebook:create_frame(name, dims)
    else
        frame = notebook:frame(id)
    end
    return frame
end

return function(baseFolder, frameSetup)
    local showoffClient = showoff.Client.new('showoff', 3000)
	local idxPath = baseFolder .. '/notebookIds.t7'

	local ids = {}
    if(path.exists(idxPath)) then
        ids = torch.load(idxPath)
    end
    

    local notebook
    if (ids.notebook ~= nil) then
        notebook = showoffClient:notebook(ids.notebook)
    else
        notebook = showoffClient:create_notebook(baseFolder)
        ids.notebook = notebook.id
    end


    local frames = {}
    for k,v in pairs(frameSetup) do
        frames[k] = loadFrame(notebook, ids[k], k, v)
        ids[k] = frames[k].id
    end

    torch.save(idxPath, ids)

    return frames
end
