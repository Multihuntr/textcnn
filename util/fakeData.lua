local cjson = require('cjson')
local tablex = require('pl.tablex')

local strToCharTens = require('data.strToCharTens')

local file = io.open('util/charDistribution.json')
local charDist = file:read("*all")
file:close()
charDist = cjson.decode(charDist)
local nChars = #charDist
local total = torch.Tensor(charDist):sum()
local cumulativeCharDist = {}
do
	local soFar = 0
	for i=1,nChars do
		soFar = soFar + charDist[i]
		cumulativeCharDist[i] = soFar
	end
end

local wordDist = torch.load('util/wordDistribution.t7')
local totalWords
local wordKeys = tablex.keys(wordDist)
local cumulativeWordDist = {}
do
	local soFar = 0
	for i=1,#wordKeys do
		soFar = soFar + wordDist[wordKeys[i]]
		cumulativeWordDist[i] = soFar
	end
	totalWords = soFar
end


-- local markov = torch.load('util/markov.t7')

local fncs = {}

function fncs.totalChars()
	return total
end

function fncs.sampleChar()
	local r = math.random(total)
	for i=1,nChars do
		if (r < cumulativeCharDist[i]) then
			return i
		end
	end
	return nChars
end


function fncs.random(minLength, maxLength)
	local len
	if (maxLength == nil) then
		len = minLength
	else
		len = math.random(maxLength-minLength) + minLength
	end
	local result = torch.CharTensor(1,len)
	local ptr = torch.data(result)
	for i=0,len-1 do
		ptr[i] = fncs.sampleChar()
	end
	return result
end


local function findWords(tens)
	local s = tens:size(1)
	local tensPtr = torch.data(tens)
	-- Records the idx of the first chars of the words
	local firsts = {0}
	-- Acts as a map from previous word order to new word order
	local mapWords = {1}

	-- Find all words
	for i=1,s-1 do
		if (firsts[#firsts] ~= i and tensPtr[i] == 1) then
			firsts[#firsts+1] = i + 1
			mapWords[#mapWords + 1] = #mapWords + 1
		end
	end

	-- Pretend like there's an extra word at the end
	--  for use in carving up the input tens
	firsts[#firsts + 1] = s+1

	return firsts, mapWords
end

local function randomSwaps(tabl, proportion)
	local n = #tabl
	for i=1,n-1 do

		if (math.random() <= proportion) then
			local temp = tabl[i+1]
			tabl[i+1] = tabl[i]
			tabl[i] = temp
		end

	end
end

local function permuteOrder(tabl, proportion)
	local n = #tabl
	local nSwaps = proportion*n
	for i=1,nSwaps do

		local idxOne = math.random(n)
		local idxTwo = math.random(n)
		while(idxTwo == idxOne) do
			idxTwo = math.random(n)
		end

		local temp = tabl[idxTwo]
		tabl[idxTwo] = tabl[idxOne]
		tabl[idxOne] = temp

	end
end


-- ========== IMPORTANT ==============
-- If Bag of Words did not work, then permuting might make sense. But, since
-- BoW is such a strong baseline, it implies that the classification of a
-- document might actually be entirely derivable from identifying key words.
-- We don't want to hinder training by suggesting that it is not.
-- ===================================
-- Char #1 is actually the space character
function fncs.permuteWords(tens, proportion)

	local firsts, mapWords = findWords(tens)
	local s = tens:size(1)


	local nWords = #mapWords
	randomSwaps(mapWords, proportion)


	-- Write out the words to a new tensor
	local tensPtr = torch.data(tens)
	local result = torch.CharTensor(s):fill(1)
	local resPtr = torch.data(result)
	local resPos = 0
	for i=1,nWords do
		local from = firsts[mapWords[i]]
		local to = firsts[mapWords[i]+1]

		for j=from,to-2 do
			resPtr[resPos] = tensPtr[j]
			resPos = resPos + 1
		end

		if (resPos <= s) then
			resPtr[resPos] = 1
			resPos = resPos + 1
		end
	end

	return result

end




-- function fncs.markov(maxLen)
--     local str = nil
--     local lastWord = "\n"
--     while str:len() < maxLen do
--         local list = markov[lastWord]

--         local sum = 0
--         for k,v in pairs(list) do
--             sum = sum + v
--         end

--         local r = math.random(sum)
--         local i = 0
--         for k,v in pairs(list) do
--             i = i + v
--             if (i >= r) then
--                 newWord = k
--                 break
--             end
--         end

--         local lastWord = newWord or "the"
--         str = (str and str .. " " or "") .. lastWord

--     end
--     return str --strToCharTens(str)
-- end


function fncs.sampleWord()

	local r = math.random(totalWords)
	for i=1,#wordKeys do
		if (r < cumulativeWordDist[i]) then
			return wordKeys[i]
		end
	end
	return wordKeys[nWords]
end


function fncs.randomWord(maxLen)
	maxLen = math.min(maxLen, math.random(maxLen*2))
	local str = fncs.sampleWord()

	while str:len() < maxLen do

		local sampled
		repeat
			sampled = fncs.sampleWord()
		until sampled ~= nil
		str = str .. " " .. sampled

	end
	return strToCharTens(str)
end


return fncs
