
local fncs = {}

local function average(tabl, skip)
	local averaged ={}
	local len = #tabl
	for i=1,len-skip,skip do
		local total = 0
		for j=1,skip do
			total = total + tabl[i+j]
		end
		table.insert(averaged, total/skip)
	end
	return averaged
end

local function averageAll(tens, points, xScale)
	local xs = {}
	local averageOver = math.ceil(#tens[1]/points)
	local domain = average(torch.FloatTensor():range(1,#tens[1]):mul(xScale):totable(), averageOver)
	local averaged = {}
	for i=1,#tens do
		xs[i] = domain

		-- Average over some points
		averaged[i] = average(tens[i], averageOver)
	end

	return xs, averaged
end

function fncs.plotLoss(frame, datasetNames, loss, yMax, nPoints, xScale)

	nPoints = nPoints or 100
	yMax = yMax or 3
	xScale = xScale or 1

	local opts = {}
	opts.series_names = datasetNames
	opts.y_min = 0
	opts.y_max = yMax
	opts.width = 1600
	opts.height = 22+250
	opts.legend_title = "Loss"

	local xs, averagedLoss = averageAll(loss, nPoints, xScale)

	for i=1,#loss do
		local tens = torch.Tensor(averagedLoss[i])
		local overMax = tens:ge(yMax)
		tens[overMax] = yMax
		averagedLoss[i] = tens:totable()
	end

	frame:graph(xs, averagedLoss, opts)
end


function fncs.plotAccuracy(frame, datasetNames, accuracy, nPoints, xScale)
	local nPoints = nPoints or 100
	xScale = xScale or 1

	local opts = {}
	opts.series_names = datasetNames
	opts.y_min = 0
	opts.y_max = 1
	opts.width = 1600
	opts.height = 22+250
	opts.legend_title = "Accuracy"

	local xs, averagedAcc = averageAll(accuracy, nPoints, xScale)

	frame:graph(xs, averagedAcc, opts)
end

return fncs
