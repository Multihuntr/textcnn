local xml = require('pl.xml')
local tnt = require('torchnet')
local zero = require('zero')
local separate = require('separate')
local str2char = require('strToCharTens')


-- local topCategories = { "U.S.", "Italia", "Health", "Europe", "Top News", "Sci/Tech", "Top Stories", "Business", "Sports", "Entertainment", "World" }
local topCategories = { "Business", "Sports", "Entertainment", "World" }
local allowedCategories = {}
for k,v in ipairs(topCategories) do
    allowedCategories[v] = k
end

local file = io.open(arg[1] .. '/newsspace200.xml', 'r')

local trainWriter = tnt.IndexedDatasetWriter("/fastdata/text/train/agNews.idx", "/fastdata/text/train/agNews.bin", "table")
local testWriter = tnt.IndexedDatasetWriter("/fastdata/text/test/agNews.idx", "/fastdata/text/test/agNews.bin", "table")


local separator = separate(30000, 1900, trainWriter, testWriter, topCategories)


file:read()
file:read()
local line = file:read()

local i = 0
while (line ~= "</all_news>") do
    i = i + 1
    if (i % 10000 == 0)then
        print("Compiling! : " .. i)
    end

    record = xml.parse('<a>' .. line .. '</a>')
    local desc = record:get_elements_with_name("description")[1][1]
    local cate = record:get_elements_with_name("category")[1][1]
    local cateIdx = allowedCategories[cate]

    if (cateIdx ~= nil and desc ~= nil) then
        local cateEnc = zero(#topCategories)
        cateEnc[cateIdx] = 1
        local entry = {['input']=str2char(desc), ['target']=cateEnc:byte()}
        separator.add(entry, cate)
    end

    line = file:read()
end

file:close()
trainWriter:close()
testWriter:close()

print(separator.getCounts())
