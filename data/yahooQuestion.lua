local xml = require('pl.xml')
local tnt = require('torchnet')
local zero = require('zero')
local separate = require('separate')
local str2char = require('strToCharTens')

local categories = {}

local topCategories = {
    "Sports",
    "Politics & Government",
    "Science & Mathematics",
    "Education & Reference",
    "Business & Finance",
    "Health",
    "Computers & Internet",
    "Society & Culture",
    "Entertainment & Music",
    "Family & Relationships"
}
local allowedCategories = {}
for k,v in ipairs(topCategories) do
    allowedCategories[v] = k
end


local file = io.open(arg[1] .. '/FullOct2007.xml', 'r')
local trainWriter = tnt.IndexedDatasetWriter("/fastdata/text/train/yahooAnswer.idx", "/fastdata/text/train/yahooAnswer.bin", "table")
local testWriter = tnt.IndexedDatasetWriter("/fastdata/text/test/yahooAnswer.idx", "/fastdata/text/test/yahooAnswer.bin", "table")

local separator = separate(140000, 5000, trainWriter, testWriter, topCategories)

file:read()
file:read()

local line = file:read()

local i = 0
while (line:find("<vespaadd>") ~= nil)  do
    i = i + 1
    if (i % 10000 == 0)then
        print("Compiling! : " .. i)
    end

    local oneOfThem = line
    repeat
        line = file:read()
        oneOfThem = oneOfThem .. line
    until(line:find("</vespaadd>") ~= nil)


    local record = xml.parse(oneOfThem)
    local bestanswer = record:get_elements_with_name("bestanswer")[1]
    local cate = record:get_elements_with_name("maincat")[1]

    if (bestanswer ~= nil and cate ~= nil) then
        bestanswer = bestanswer[1]
        cate = cate[1]
        local cateIdx = allowedCategories[cate]
        if (cateIdx ~= nil) then
            local cateEnc = zero(#topCategories)
            cateEnc[cateIdx] = 1
            local str = str2char(bestanswer)
            if (str:le(0):sum() > 0) then
                print(bestanswer)
                print(str)
                error("invalid char")
            end
            local entry = {['input']=str, ['target']=cateEnc:byte()}
            separator.add(entry, cate)
        end
    end



    -- Sometimes there is a newline between records, sometimes not.
    local st, en = line:find("</vespaadd>")
    line = line:sub(en+1)
    if (line:find("<vespaadd>") == nil) then
        line = file:read()
    end
end

file:close()
trainWriter:close()
testWriter:close()

print(separator:getCounts())
