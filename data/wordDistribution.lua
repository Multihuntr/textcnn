local xml = require('pl.xml')
local cjson = require('cjson')

local words = {}

local function insert(idx)
	if (words[idx] == nil) then
		words[idx] = 0
	end
	words[idx] = words[idx]+1
end

local i = 0
local DEFAULT_FIRST_WORD = "\n"
local lastWord
local function updateWordPairs(str)
	if (str == nil) then
		return
	end
	for w in str:gmatch("%S+") do
		i = i + 1
		if (i % 1000000 == 0) then
			print(string.format("%010d done", i))
			collectgarbage()
		end
		insert(w)
	end
end


local MAX_WORDS = 20000000


-- AG's News Corpus
do

	local topCategories = { "Business", "Sports", "Entertainment", "World" }
	local allowedCategories = {}
	for k,v in ipairs(topCategories) do
			allowedCategories[v] = k
	end

	local file = io.open(arg[1] .. '/agNews/newsspace200.xml', 'r')

	file:read()
	file:read()
	local line = file:read()

	local count = i
	while (line ~= "</all_news>" and i < count + MAX_WORDS) do

		local record = xml.parse('<a>' .. line .. '</a>')
		local desc = record:get_elements_with_name("description")[1][1]
		local cate = record:get_elements_with_name("category")[1][1]

		if (allowedCategories[cate]) then
			updateWordPairs(desc)
		end

		line = file:read()
		
	end

	file:close()
end
print("Progress!!!!!")
torch.save('../util/wordDistribution.t7', words)

-- Amazon Reviews
do
	local file = io.open(arg[1] .. '/amazonReviews/all.txt', 'r')

	local function removeStart(str)
		local idx = str:find(":")
		return str:sub(idx + 2)
	end

	local line = file:read()

	local count = i
	while (line ~= nil and i < count + MAX_WORDS) do

		file:read()
		file:read()
		file:read()
		file:read()
		file:read()
		file:read()
		file:read()
		file:read()
		local text = removeStart(file:read())
		file:read()

		updateWordPairs(text)

		line = file:read()

	end

	file:close()
end
print("Progress!!!!!")
torch.save('../util/wordDistribution.t7', words)

-- Yahoo Question Answer
do
	local topCategories = {
			"Sports",
			"Politics & Government",
			"Science & Mathematics",
			"Education & Reference",
			"Business & Finance",
			"Health",
			"Computers & Internet",
			"Society & Culture",
			"Entertainment & Music",
			"Family & Relationships"
	}
	local allowedCategories = {}
	for k,v in ipairs(topCategories) do
			allowedCategories[v] = k
	end
	local file = io.open(arg[1] .. '/yahooQuestionAnswer/FullOct2007.xml', 'r')

	file:read()
	file:read()

	local line = file:read()

	local count = i
	while (line:find("<vespaadd>") ~= nil and i < count + MAX_WORDS)  do

		local oneOfThem = line
		repeat
				line = file:read()
				oneOfThem = oneOfThem .. line
		until(line:find("</vespaadd>") ~= nil)
		local record = xml.parse(oneOfThem)
		local bestanswer = record:get_elements_with_name("bestanswer")[1]
		local cate = record:get_elements_with_name("maincat")[1]

		if (bestanswer ~= nil and cate ~= nil) then
			bestanswer = bestanswer[1]
			cate = cate[1]
			if (allowedCategories[cate]) then
				updateWordPairs(bestanswer)
			end
		end

		local st, en = line:find("</vespaadd>")
		line = line:sub(en+1)
		if (line:find("<vespaadd>") == nil) then
				line = file:read()
		end

	end

	file:close()
end
print("Progress!!!!!")
torch.save('../util/wordDistribution.t7', words)

-- YelpReviews
do 
	local file = io.open(arg[1] .. '/yelpReviews/yelp_academic_dataset_review.json', 'r')

	local line = file:read()

	local count = i
	while (line ~= nil and i < count + MAX_WORDS) do

			local asJson = cjson.decode(line)

			updateWordPairs(asJson.text)

			line = file:read()

	end

	file:close()
end
print("Progress!!!!!")
torch.save('../util/wordDistribution.t7', words)
