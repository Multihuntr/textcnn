local parallelIterator = require('parallelIterator')
local strToCharTens = require('strToCharTens')

local idx = 1
-- A map from the char's integer representation to an index
local mapToIdx = {}
-- A count of how many of each char appeared, indexed by char integer representation
local charCount = {}
-- Just to see which char that is, again
local charChar = {} -- Charmander!
local count = 0

local function charToIdx(tens)
    print({tens})
    if (type(tens) == "table") then
        tens = tens[1]
        if (type(tens) == "userdata") then
            
        end
    end

    print({tens})
    if (type(tens) == "string") then
        tens = strToCharTens(tens)
    end
    print({tens})
    local len = tens:size(2);
    for i=1,len do
        local a = tens[{1, i}]
        if (mapToIdx[a] == nil) then
            mapToIdx[a] = idx
            charCount[a] = 1
            idx = idx + 1
            -- print(a)
            charChar[a] = string.char(a+31)
        else 
            charCount[a] = charCount[a] + 1
        end
    end
end

local function countChars(iter)
    for i=1,1000 do
        count = count  +1 
        -- print(count)
        if (i%100 == 0) then
            print("Progress")
        end
        local sample = iter()
        charToIdx(sample.input)
    end
    print("More progress")
end


local trainDatasetFolder = "/data/text/train/"
local testDatasetFolder = "/data/text/test/"
-- Parallel lists
local datasetNames = {"agNews", "amazonReviews", "yahooAnswer", "yelpReviews"}

for k,v in pairs(datasetNames) do
    print("=========== " .. v .. " ===========")
    local trainIter = parallelIterator(trainDatasetFolder, v, 2)
    countChars(trainIter)
    local testIter = parallelIterator(testDatasetFolder, v, 2)
    countChars(testIter)
end

print(mapToIdx)
print(charCount)
print(charChar)
