local tnt = require('torchnet')
local cjson = require('cjson')
local zero = require('zero')
local separate = require('separate')
local str2char = require('strToCharTens')

local file = io.open(arg[1] .. '/yelp_academic_dataset_review.json', 'r')
local trainWriter = tnt.IndexedDatasetWriter("/fastdata/text/train/yelpReviews.idx", "/fastdata/text/train/yelpReviews.bin", "table")
local testWriter = tnt.IndexedDatasetWriter("/fastdata/text/test/yelpReviews.idx", "/fastdata/text/test/yelpReviews.bin", "table")

local separator = separate(130000, 10000, trainWriter, testWriter, {1,2,3,4,5})

local function removeStart(str)
    local idx = str:find(":")
    return str:sub(idx + 2)
end

local line = file:read()

local i = 0
while (line ~= nil) do
    i = i + 1
    if (i % 10000 == 0)then
        print("Compiling! : " .. i)
    end

    local asJson = cjson.decode(line)


    local starsEnc = zero(5)
    starsEnc[{asJson.stars}]=1
    local entry = {['input']=str2char(asJson.text), ['target']=starsEnc:byte()}
    separator.add(entry, asJson.stars)

    line = file:read()
end

file:close()
trainWriter:close()
testWriter:close()

print(separator.getCounts())
