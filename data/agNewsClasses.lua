local xml = require('pl.xml')

local categories = {}

local file = io.open(arg[1] .. '/newsspace200.xml', 'r')

file:read()
file:read()
local line = file:read()

local i = 0
while (line ~= "</all_news>") do
    i = i + 1
    if (i % 10000 == 0)then
        print("Reading! : " .. i)
    end

    local record = xml.parse('<a>' .. line .. '</a>')
    local cate = record:get_elements_with_name("category")[1][1]

    if (cate ~= nil) then
        if (categories[cate] == nil) then
            categories[cate] = 0
        end
        categories[cate] = categories[cate] + 1
    end
    
    line = file:read()
end

file:close()

print(categories)
