local tnt = require('torchnet')
local zero = require('zero')
local separate = require('separate')
local str2char = require('strToCharTens')

local file = io.open(arg[1] .. '/all.txt', 'r')
local trainWriter = tnt.IndexedDatasetWriter("/fastdata/text/train/amazonReviewsPolarity.idx", "/fastdata/text/train/amazonReviewsPolarity.bin", "table")
local testWriter = tnt.IndexedDatasetWriter("/fastdata/text/test/amazonReviewsPolarity.idx", "/fastdata/text/test/amazonReviewsPolarity.bin", "table")
-- local polWriter = tnt.IndexedDatasetWriter(arg[1] .. "/amazonReviewsPolarity.idx", arg[1] .. "/amazonReviewsPolarity.bin", "table")

-- Is in the following form:
-- product/productId
-- product/title
-- product/price
-- review/userId
-- review/profileName
-- review/helpfulness
-- review/score
-- review/time
-- review/summary
-- review/text

-- local separator = separate(1800000, 200000, trainWriter, testWriter, {1, 2})
local separator = separate(600000, 130000, trainWriter, testWriter, {1,2,3,4,5})

local function removeStart(str)
    local idx = str:find(":")
    return str:sub(idx + 2)
end

local line = file:read()

local i = 0
while (line ~= nil) do
    i = i + 1
    if (i % 10000 == 0)then
        print("Compiling! : " .. i)
    end

    file:read()
    file:read()
    file:read()
    file:read()
    file:read()
    local score = tonumber(removeStart(file:read()))
    file:read()
    file:read()
    local text = removeStart(file:read())
    file:read()


    local scoreEnc = zero(5)
    scoreEnc[{score}]=1
    -- local scoreEnc = zero(2)
    -- if (score ~= 3) then
    --     local posNeg = (score > 3) and 1 or 2
    --     scoreEnc[{posNeg}]=1
    if (text ~= "") then
        local entry = {['input']=str2char(text), ['target']=scoreEnc:byte()}
        separator.add(entry, score)
    end
    -- end

    -- if (score > 3 or score < 3) then
    --     local pol = zero(2)
    --     local polIdx = ((score > 3) and 2 or 1)
    --     pol[polIdx] = 1
    --     polWriter:add({['input']=text, ['target']=pol:byte()})
    -- end

    line = file:read()
end

file:close()
trainWriter:close()
testWriter:close()

print(separator.getCounts())

