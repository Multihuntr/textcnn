
local myInit = function()
    require('torch')
    tnt = require('torchnet')
    tablex = require('pl.tablex')
    math.randomseed(123)
    torch.manualSeed(123)
end

local function newIter(location, dataset, batchSize)
    local tnt = require('torchnet')
    local indexf = location .. '/' .. dataset .. '.idx'
    local dataf = location .. '/' .. dataset .. '.bin'

    -- Note that the ParallelDatasetIterator function would normally return a function that returns
    --  and iterator. To make it nicer elsewhere, I am simply immediately calling that function and
    --  getting an iterator returned
    return tnt.ParallelDatasetIterator({
        ['init'] = myInit,
        ['closure'] = function()
            local dataReader = tnt.IndexedDatasetReader({indexfilename=indexf, datafilename=dataf})
            local range = tablex.range(1,dataReader:size())

            local listData = tnt.ListDataset({
                list=range,
                load=function(i)
                    local a = dataReader:get(i)
                    return a
                end
            })
            -- local data = tnt.IndexedDataset({['fields'] = {dataset}, ['path'] = location})
            local shuffledData = tnt.ShuffleDataset({['dataset'] = listData, ['replacement'] = true})
            return tnt.BatchDataset({['dataset'] = shuffledData, ['batchsize'] = batchSize})
        end,
        ['nthread'] = 1
    })()

end

return function(location, dataset, batchSize, infiniteSample)
    local iter = newIter(location, dataset, batchSize)

    if (infiniteSample) then

        return function()
            local sample = iter()
            if (sample == nil or sample.input:size(1) < batchSize) then
                iter = newIter(location, dataset, batchSize)
                sample = iter()
            end
            return sample
        end
    else
        return iter
    end
end
