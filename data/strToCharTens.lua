
-- The nice range of chars used is from #32-#126

-- The chars in #0-#31 seem to be mostly unused or kinds of whitespace
--  E.g. Char #10, #11 and #12 seem to be different new-line symbols which fall outside
--   the 'nice range' of characters.
--  All of these will be mapped to #128, and the 'nice' range will remain the same.

-- Some characters are represented as multiple char values for some reason. These include
--   all those funky characters that are in Unicode, and some formatting characters.
-- These are all greater than 128, so they will all be mapped to #129

-- So, to move the chars to their indexes, the values of the chars are moved down by 31
--  to use up the first 31 indices.
-- This enforces that the number of the possible characters is: 128-31=97



local function fixSize(size, tens)
    if (tens:size(1) > size) then
        return tens[{{1, size}}]
    end
    local replacementTens = torch.CharTensor(size):fill(1)
    replacementTens[{{1, tens:size(1)}}] = tens
    return replacementTens
end

return function(str)
    local stor = torch.CharStorage():string(str)
    local tens = torch.CharTensor(stor)

    tens:apply(function(val) 
        if (val < 32) then
            return 128
        end
        if (val > 127) then
            return 129
        end
        return val
    end):csub(31)

    return fixSize(1014, tens)
end
