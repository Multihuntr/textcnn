local xml = require('pl.xml')

local categories = {}

local file = io.open(arg[1] .. '/FullOct2007.xml', 'r')

file:read()
file:read()

local line = file:read()

local i = 0
while (line:find("<vespaadd>") ~= nil)  do
    i = i + 1
    if (i % 10000 == 0)then
        print("Reading! : " .. i)
    end

    local oneOfThem = line
    repeat
        line = file:read()
        oneOfThem = oneOfThem .. line
    until(line:find("</vespaadd>") ~= nil)


    local record = xml.parse(oneOfThem)
    local cate = record:get_elements_with_name("maincat")[1]

    if (cate ~= nil) then
        cate = cate[1]
        if (categories[cate] == nil) then
            categories[cate] = 0
        end
        categories[cate] = categories[cate] + 1
    end
    
    local st, en = line:find("</vespaadd>")
    line = line:sub(en+1)
    if (line:find("<vespaadd>") == nil) then
        line = file:read()
    end
end

file:close()

print(categories)
