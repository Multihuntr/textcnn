-- Returns a function that will assign an entry into a category in either test or train.
-- This is mostly used to ensure that the number of examples from each category is equal,
--  and to automatically create the test/train split

return function (TRAIN_MAX, TEST_MAX, trainWriter, testWriter, categories)

    local countC = {}
    for k,v in ipairs(categories) do
        countC[v] = {}
        countC[v].train = 0
        countC[v].test = 0
    end


    local ratio = math.floor(TRAIN_MAX/TEST_MAX)+1


    local fncs = {}

    function fncs.add(entry, c)
        if (countC[c].train >= TRAIN_MAX and countC[c].test >= TEST_MAX) then
            -- All full!
            return
        end

        local choice = math.random(ratio)
        if ((choice == 1 or countC[c].train >= TRAIN_MAX) and countC[c].test < TEST_MAX) then
            countC[c].test = countC[c].test + 1
            testWriter:add(entry)
        else
            countC[c].train = countC[c].train + 1
            trainWriter:add(entry)
        end
    end

    function fncs.getCounts()
        return countC
    end

    return fncs
end
