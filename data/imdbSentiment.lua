local plDir = require('pl.dir')

local alphabetSize = 26

local a = "a"
local byteStart = a:byte()-1

local specialsMap ={}
local specials = { "0","1","2","3","4","5","6","7","8","9",",",";",".","!","?",":","/","|","_","@","#","$","%","^","&","*","+","=","<",">","(",")","{","}"," " }
for k,v in ipairs(specials) do
	specialsMap[v:byte()] = alphabetSize+1+byteStart
	alphabetSize = alphabetSize+1
end
print(alphabetSize-1)

local removePattern = "[^a-z"
for k, v in pairs(specials) do
	removePattern = removePattern .. v
end
removePattern = removePattern .. ']'

local function charIndices(str)
	str = str:lower()
	-- There seems to a problem in lua using byte on strings longer than
	--  a few thousand characters long. So I'm just splitting it up
	--  and then rejoining them.
	if (str:len() > 2500) then
		local left = charIndices(str:sub(1,2500))
		local right = charIndices(str:sub(2501, str:len()))
		return left:cat(right)
	else
		local strEnc = {str:byte(1, str:len())}
		local indices = torch.ByteTensor(strEnc)
		for k,v in pairs(specialsMap) do
			indices[indices:eq(k)] = v
		end
		
		return indices:csub(byteStart)
	end
end

local function oneHot(str)
	local indices = charIndices(str)
	local s = indices:size(1)
	local len = tonumber(arg[1]) or s
	local encoded = torch.ByteTensor(len, alphabetSize):zero()
	for i=1,math.min(len, s) do
		encoded[{{i}, {indices[i]}}] = 1
	end
	return encoded
end

local function readFile(filename)
	local file = io.open(filename, 'r')
	local result = file:read()
	file:close()
	local cleaned = result:gsub("<br %/><br %/>", " ")
						:gsub(removePattern, "")
	return oneHot(cleaned)
end

local function readDir(dir)
	local files = plDir.getfiles(dir)
	local encodedFiles = arg[1] and torch.ByteTensor(#files, tonumber(arg[1]), alphabetSize) or {}	
	local i = 0
	for k,v in ipairs(files) do
		local lastSlash = v:len()+1-v:reverse():find('%/')
		local lastDot = v:len()+1-v:reverse():find('_')
		local ind = tonumber(v:sub(lastSlash+1, lastDot-1))
		i = i + 1
		print (i)
		encodedFiles[ind+1] = readFile(v)
	end
	return encodedFiles
end

local baseDir = './aclImdb/'
local savePrefix = arg[1] and arg[1] .. '_' or ''
do
	local train = {}
	train.neg = readDir(baseDir .. 'train/neg')
	train.pos = readDir(baseDir .. 'train/pos')
	torch.save(savePrefix .. "train.t7", train)
end
collectgarbage()
collectgarbage()
do
	local test = {}
	test.neg = readDir(baseDir .. 'test/neg')
	test.pos = readDir(baseDir .. 'test/pos')
	torch.save(savePrefix .. "test.t7", test)
end

