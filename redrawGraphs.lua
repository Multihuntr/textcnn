local folder = arg[1]
local loadFrames = require('util.loadShowoffFrames')
local visualisation = require('util.visualisation')

local showoff = require('showoff')
local showoffClient = showoff.Client.new('showoff', 3000)


local losses = torch.load(folder .. '/losses.t7')
local accuracies = torch.load(folder .. '/accuracies.t7')
local ids = torch.load(folder .. '/notebookIds.t7')


local datasetNames = {"agNews", "amazonReviews", "yahooAnswer", "yelpReviews"}
local frameSetup = { trainLosses = {x=0, y=0, width=1500, height=250},
                     valLosses = {x=0, y=250, width=1500, height=250},
                     accuracy = {x=0, y=500, width=1500, height=250} }

local visualise = loadFrames(folder, frameSetup)

visualisation.plotLoss(visualise.trainLosses, datasetNames, losses.train, 3, 100)
visualisation.plotLoss(visualise.valLosses, datasetNames, losses.val, 3, 100)
visualisation.plotAccuracy(visualise.accuracy, datasetNames, accuracies, 30, 20)
