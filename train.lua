require('cunn')

local mkUpdate = require('util.makeUpdate')
local visualisation = require('util.visualisation')

local function saveModel(folder, body, heads)
	body:clearState()
	for idx,head in pairs(heads) do
		head:clearState()
	end
	torch.save(folder .. '/body.t7', body)
	torch.save(folder .. '/heads.t7', heads)
end

-- Takes the values from summedLoss for each each, and averages over the
--  number of added values, and adds to the loss table
local function recordLoss(heads, loss, summedLoss, recordEvery)
	for idx,head in pairs(heads) do
		loss[idx][#loss[idx] + 1] = summedLoss[idx]/recordEvery
		summedLoss[idx] = 0
	end
end

local function getValLoss(heads, validate, testIters, recordEvery)
	local summedLoss = {}
	for idx,head in pairs(heads) do
		summedLoss[idx] = 0
	end
	
	for i=1,recordEvery do
		local valLosses = validate(testIters)
		for idx,head in pairs(heads) do
			summedLoss[idx] = summedLoss[idx] + valLosses[idx]
		end
	end

	return summedLoss
end

local function testAccuracy(body, head, iter)

	head:evaluate()

	local correct = 0
	local batchSize
	local times = 2

	for i=1,times do
		local sample = iter()
		batchSize = sample.input:size(1)

		local rawPrediction = head:forward(body:forward(sample.input))
		local _, pred = rawPrediction:max(2)

		correct = correct + pred:eq(sample.target:type('torch.CudaLongTensor')):sum()
	end

	head:training()

	return correct/(times*batchSize)
end

return function(iters, testIters, body, datasetNames, heads, loss, accuracies, opts, visualiseFrames)

	local summedLoss = {}


	local criterion = nn.CrossEntropyCriterion()
	criterion:cuda()
	
	body:training()
	body:cuda()
	local update, validate = mkUpdate(body, heads, criterion, opts)

	for idx,head in pairs(heads) do

		head:training()
		head:cuda()
	
		summedLoss[idx] = 0
	end


	-- Pre-defining the directories to save the losses and accuracies
	local lossDir = opts.saveFolder .. '/losses.t7'
	local accDir = opts.saveFolder .. '/accuracies.t7'



	xlua.progress(0, opts.trainTime)
	-- j is a counter of the number of training steps taken
	local j = #loss.train[1] or 0
	while(j < opts.trainTime) do

		-- Perform a step of updating the model
		local stepLosses = update(iters)
		for idx,v in pairs(stepLosses) do
			summedLoss[idx] = summedLoss[idx] + stepLosses[idx]
		end

		xlua.progress(j, opts.trainTime)

		-- Record loss
		if (j % opts.recordEvery == 0) then
			recordLoss(heads, loss.train, summedLoss, opts.recordEvery)

			local summedValLoss = getValLoss(heads, validate, testIters, opts.recordEvery)

			recordLoss(heads, loss.val, summedValLoss, opts.recordEvery)
		end

		-- Test Accuracy
		if (j % opts.testEvery == 0) then
			body:evaluate()
			for idx,head in pairs(heads) do
				table.insert(accuracies[idx], testAccuracy(body, head, testIters[idx]))
			end
			body:training()
		end

		-- Display loss
		if (opts.displayEvery ~= 0 and j % opts.displayEvery == 0) then
			visualisation.plotLoss(visualiseFrames.trainLosses, datasetNames, loss.train)
			visualisation.plotLoss(visualiseFrames.valLosses, datasetNames, loss.val)
			visualisation.plotAccuracy(visualiseFrames.accuracy, datasetNames, accuracies)
		end

		-- Save loss to disk
		if (opts.saveEvery ~= 0 and j % opts.saveEvery == 0) then
			saveModel(opts.saveFolder, body, heads)
			torch.save(lossDir, loss)
			torch.save(accDir, accuracies)
		end


		j = j + 1

	end

	xlua.progress(opts.trainTime, opts.trainTime)

	saveModel(opts.saveFolder, body, heads)
	torch.save(lossDir, loss)

	return body, heads

end
