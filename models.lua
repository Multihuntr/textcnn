require('nn')
require('cudnn')

local alphabetSize = 97


local fncs = {}

-- Outer storage for keeping track of current length
local l = 0


-- d = dimension size
-- w = window size
-- s = stride size
-- p = padding size
local function outDimCalc(d, w, s, p)
	s = s or 1
	p = p or 0
	return math.floor((d + 2*p - w)/s + 1)
end


local function addConv(container, inMaps, outMaps, convSize, stepSize)
	local pad = math.floor(convSize/2)
	container:add(nn.Padding(2, -pad))
	container:add(nn.Padding(2, pad))
	container:add(nn.TemporalConvolution(inMaps, outMaps, convSize, stepSize))
	l = outDimCalc(l, convSize, 1, pad)

	-- There's no TemporalBatchNormalisation in Torch :(
	-- Instead, it is treated as an image with a singleton dimension
	container:add(nn.Transpose({2, 3}))
	container:add(nn.View(outMaps, l, 1))
	container:add(nn.SpatialBatchNormalization(outMaps))
	container:add(nn.View(outMaps, l))
	container:add(nn.Transpose({2, 3}))
	container:add(nn.ELU(1, true))
end

local function mkMax(...)
	local params = {...}
	l = outDimCalc(l, params[1], params[2])
	return nn.TemporalMaxPooling(...)
end

function fncs.one(lastLen)
	local model = nn.Sequential()

	local lastSize = alphabetSize
	local nextSize = 16

	for i=1,4 do
		addConv(model, lastSize, nextSize, 15)
		model:add(nn.TemporalDynamicKMaxPooling(lastLen, 0.6))
		lastSize = nextSize
		nextSize = nextSize*2
	end
	addConv(model, lastSize, nextSize, 15)
	model:add(nn.TemporalDynamicKMaxPooling(lastLen))

	-- print(nextSize*lastLen)

	model:add(nn.View(nextSize*lastLen))
	model:add(nn.Linear(nextSize*lastLen, 1))
	model:add(nn.Sigmoid())

	return model
end

function fncs.two(lastLen)
	local model = nn.Sequential()

	local maps = {256, 256, 256, 512, 1024}

	addConv(model, alphabetSize, maps[1], 9)
	model:add(nn.TemporalDynamicKMaxPooling(lastLen, 0.5))
	for i=1,4 do
		addConv(model, maps[i], maps[i+1], 5)
		model:add(nn.TemporalDynamicKMaxPooling(lastLen, 0.4))
	end
	addConv(model, maps[5], maps[5], 5)
	model:add(nn.TemporalDynamicKMaxPooling(lastLen))

	-- print(nextSize*lastLen)

	local linear = 1024

	model:add(nn.View(maps[5]*lastLen))
	model:add(nn.Linear(maps[5]*lastLen, linear))
	model:add(nn.ELU(1, true))
	model:add(nn.Linear(linear, 1))
	model:add(nn.Sigmoid())

	return model
end

-- Zhang models based on what is used in 
--   "Character-level Convolutional Networks for Text Classification"
--  by Zhang et. al.
-- Published Arxiv 04/04/2014
function fncs.zhang20140404(firstLen, maps, linear)
	l = firstLen

	local model = nn.Sequential()

	addConv(model, alphabetSize, maps, 7)
	model:add(mkMax(3, 3))
	addConv(model, maps, maps, 7)
	model:add(mkMax(3, 3))
	for i=1,3 do
		addConv(model, maps, maps, 3)
	end
	addConv(model, maps, maps, 3)
	model:add(mkMax(3, 3))

	-- print(l)
	
	model:add(nn.View(maps*l))
	model:add(nn.Linear(maps*l, linear))
	model:add(nn.ELU(1, true))
	model:add(nn.Linear(linear, linear))
	model:add(nn.ELU(1, true))
	model:add(nn.Linear(linear, 1))
	model:add(nn.Sigmoid())


	return model
end

function fncs.zhang20140404_small()
	return fncs.zhang20140404(1014, 256, 1024)
end


function fncs.zhang20140404_large()
	return fncs.zhang20140404(1014, 1024, 2048)
end

-- Modified versions have DynamicKMaxPooling instead
function fncs.modifiedZhang20140404(lastLen, maps, linear)
	local model = nn.Sequential()

	addConv(model, featureSize, maps, 7)
	model:add(nn.TemporalDynamicKMaxPooling(lastLen, 0.33))
	addConv(model, maps, maps, 7)
	model:add(nn.TemporalDynamicKMaxPooling(lastLen, 0.33))
	for i=1,3 do
		addConv(model, maps, maps, 3)
	end
	addConv(model, maps, maps, 3)
	model:add(nn.TemporalDynamicKMaxPooling(lastLen))

	model:add(nn.View(maps*lastLen))
	model:add(nn.Linear(maps*lastLen, linear))
	model:add(nn.ELU(1, true))
	model:add(nn.Linear(linear, linear))
	model:add(nn.ELU(1, true))
	model:add(nn.Linear(linear, 1))
	model:add(nn.Sigmoid())


	return model
end

function fncs.modifiedZhang20140404_small(lastLen)
	return fncs.zhang20140404(lastLen, 256, 1024)
end


function fncs.modifiedZhang20140404_large(lastLen)
	return fncs.zhang20140404(lastLen, 1024, 2048)
end

function fncs.theHydra(firstLen, outputSizes, allLinearInHead)
	l = firstLen

	local maps = 256
	local linear = 2048

	local body = nn.Sequential()

	body:add(nn.LookupTable(97, 16))    
	addConv(body, 16, maps, 7)
	body:add(mkMax(3, 3))
	addConv(body, maps, maps, 7)
	body:add(mkMax(3, 3))
	for i=1,3 do
		addConv(body, maps, maps, 3)
	end
	addConv(body, maps, maps, 3)
	body:add(mkMax(3, 3))
	body:add(nn.View(maps*l))

	if (allLinearInHead == false) then
		body:add(nn.Linear(maps*l, linear))
		body:add(nn.BatchNormalization(linear))
		body:add(nn.ELU(1, true))
	end

	-- From the poisonous mists of the great swamp, I summon the great and mangey heads of the hideous beast of legend: The Hydra
	local heads = {}

	for k,v in pairs(outputSizes) do
		heads[k] = nn.Sequential()

		if (allLinearInHead) then
			heads[k]:add(nn.Linear(maps*l, linear))
			heads[k]:add(nn.BatchNormalization(linear))
			heads[k]:add(nn.ELU(1, true))
		end

		heads[k]:add(nn.Linear(linear, linear))
		heads[k]:add(nn.BatchNormalization(linear))
		heads[k]:add(nn.ELU(1, true))
		heads[k]:add(nn.Linear(linear, v+1))
		-- heads[k]:add(nn.Sigmoid())
	end


	return body, heads
end


return fncs
