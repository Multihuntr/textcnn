local tnt = require('torchnet')
local tablex = require('pl.tablex')

local function countCorrect(body, head, sample, outputSize)

    local inp = sample.input:cuda()
    -- print(inp)

    local rawPrediction = head:forward(body:forward(inp))
    local _, pred = rawPrediction[{{}, {1,outputSize}}]:max(2)
    local _, targ = sample.target:cuda():max(2)

    return pred:eq(targ):sum()

end

return function(testIters, body, heads, outputSizes, opts)
    body:evaluate()
    local resultOut = io.open(opts.saveFolder .. '/result.txt', 'w')


    for idx,head in pairs(heads) do
        head:evaluate()

        local nCorrect = 0
        local iterations = 0
        local sample = testIters[idx]()
        while (sample ~= nil) do
            iterations = iterations + 1
            if (iterations % math.floor(1000/8) == 0) then
                print(iterations .. " done")
            end
            nCorrect = nCorrect + countCorrect(body, head, sample, outputSizes[idx])
            sample = testIters[idx]()
        end

        resultOut:write('Testing for #' .. idx .. '\n'
                    ..  '#Correct:  ' .. nCorrect .. '\n'
                    ..  '#Tested:   ' .. iterations*8 .. '\n'
                    ..  '%Correct:  ' .. nCorrect/(iterations*8)*100 .. '\n\n')

    end

    resultOut:close()

end
