
local train = require('train')
local test = require('testModel')
local models = require('models')

local parallelIterator = require('data.parallelIterator')

local plPath = require('pl.path')
local plUtils = require('pl.utils')

local loadFrames = require('util.loadShowoffFrames')
local fakeData = require('util.fakeData')


local cmd = torch.CmdLine()
cmd:option('-saveFolder'     , '', 'Specify a folder (will generate one if none provided)')
cmd:option('-modelName'      , 'theHydra', 'The model name (i.e. the function in the file)')
cmd:option('-justTest'       , false, 'Should it just try to test without any training')

cmd:option('-fixedSizeInput' , 1014, 'The fixed-size length data input')
cmd:option('-minSeq'         , 60, 'The minimum length of sequence used in the model')

cmd:option('-recordEvery'    , 1, 'How often the loss should be recorded')
cmd:option('-saveEvery'      , 20, 'How often the loss and models should be saved to disk')
cmd:option('-displayEvery'   , 0, 'How often to refresh statistics')
cmd:option('-testEvery'      , 20, 'How often to get a test accuracy')
cmd:option('-trainTime'      , 30000, 'How long to train (minibatch iterations)')

cmd:option('-minibatchSize'  , 64, 'How many in a minibatch')

cmd:option('-lr'             , 1e-3, 'Learning rate')
cmd:option('-lrDecay'        , 0, 'Learning rate decay')
cmd:option('-weightDecay'    , 0, 'Weight decay')
cmd:option('-learningAlg'    , 'adadelta', '[torch:optim] Learning algorithm from optim')
cmd:option('-propFake'       , 0.25, 'Proportion of minibatch to be fake data')
cmd:option('-updateAfterEach', false, 'Whether to update the bodies weights after each head')
cmd:option('-allLinearInHead', false, 'Include no linear layers in body')

cmd:option('-datasetFolder'  , '/fastdata/text/', 'Location of dataset index files')
cmd:option('-datasetNames'   , 'agNews,amazonReviews,yahooAnswer,yelpReviews', 'Comma separated list of datasets to use')
cmd:option('-outputSizes'    , '4,5,10,5', 'Comma separated list parallel to datasetNames that indicates the number of outputs to expect')

cmd:option('-randomSeed'     , 1868, 'Manually set the random seed')

local opts = cmd:parse(arg)

-- Assign some static values
torch.setdefaulttensortype('torch.CudaTensor');
torch.manualSeed(opts.randomSeed)
math.randomseed(opts.randomSeed)



-- Set up data iterators
local trainDatasetFolder = opts.datasetFolder .. "/train/"
local testDatasetFolder = opts.datasetFolder .. "test/"
-- Parallel lists
local datasetNames = plUtils.split(opts.datasetNames, ",")
local outputSizes = plUtils.split(opts.outputSizes, ",")
for k,v in pairs(outputSizes) do
    outputSizes[k] = tonumber(v)
end

local trainIters = {}
local valIters = {}
local testIters = {}

local loss = {}
loss.train = {}
loss.val = {}
local accuracies = {}
local nFake = math.floor(opts.minibatchSize*opts.propFake)
local nReal = opts.minibatchSize-nFake

local function mkBatchIterWithFake(folder, name, nTarg)
    local baseIter = parallelIterator(folder, name, nReal, true)
    local input = torch.Tensor(opts.minibatchSize, opts.fixedSizeInput)
    local target = torch.Tensor(opts.minibatchSize)

    return function()
        local real = baseIter()
        input[{{1,nReal}}] = real.input
        local _, targetIdx = real.target:max(2)
        target[{{1,nReal}}] = targetIdx:squeeze()

        for i=nReal+1, opts.minibatchSize do
            input[{{i}}] = fakeData.randomWord(opts.fixedSizeInput)
        end

        target[{{nReal+1, opts.minibatchSize}}] = nTarg+1

        return {['input']=input, ['target']=target}
    end
end


for k,v in pairs(datasetNames) do
    trainIters[k] = mkBatchIterWithFake(trainDatasetFolder, v, outputSizes[k]+1)
    valIters[k] = mkBatchIterWithFake(testDatasetFolder, v, outputSizes[k]+1)
    testIters[k] = parallelIterator(testDatasetFolder, v, 8)
    loss.train[k] = {}
    loss.val[k] = {}
    accuracies[k] = {}
end


-- Set up folders, model and loss (or load previous if possible)
local body, heads = models[opts.modelName](opts.fixedSizeInput, outputSizes, opts.allLinearInHead)
if (opts.saveFolder == "" or plPath.exists(opts.saveFolder .. '/body.t7') == false) then
    -- Make new folder and model
    local startDateTime = os.date('%Y-%m-%d_%X')
    local folderName = 'bin/' .. opts.modelName .. '_' .. startDateTime .. '/'

    opts.saveFolder = folderName
    paths.mkdir(opts.saveFolder)
else
    -- Continue from before
    body = torch.load(opts.saveFolder .. '/body.t7')
    heads = torch.load(opts.saveFolder .. '/heads.t7')
    loss = torch.load(opts.saveFolder .. '/losses.t7')
    accuracies = torch.load(opts.saveFolder .. '/accuracies.t7')
end

cmd:log(opts.saveFolder .. '/opts.txt', opts);





-- Actually perform training and testing
if (opts.justTest == false) then
    local visualise
    if (opts.displayEvery ~= 0) then
        -- Set up the notebook frames for visualisation
        local frameSetup = { trainLosses = {x=0, y=0, width=1500, height=250},
                            valLosses = {x=0, y=250, width=1500, height=250},
                            accuracy = {x=0, y=500, width=1500, height=250} }
        visualise = loadFrames(opts.saveFolder, frameSetup)
    end

    body, heads = train(trainIters, valIters, body, datasetNames, heads, loss, accuracies, opts, visualise)
end
test(testIters, body, heads, outputSizes, opts)
