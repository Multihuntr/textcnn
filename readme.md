# Some Character-level Text CNNs

This repository has several ways of training some character-level CNNs for classification or sentiment analysis. The architecture is based on:

[Character-level Convolutional Networks for Text Classification](https://arxiv.org/abs/1509.01626)

Note: there is some significant set-up required. This repository uses:
Docker, and torch7 (and some libraries)
See [set-up section](#set-up)

## Normal mode

Can train a normal convolutional neural network with nothing special with the command:

```
th main.lua -modelName zhang20140404_small -datasetFolder <folder containing dataset>-datasetNames agNews -outputSizes 4 -propFake 0 
```

(note: datasetNames can accept a comma separated list, and outputSizes is considered a parallel list; make sure you have the same number of both and they align correctly)

(note: the data format should be an indexed dataset created through torchnet, see [Datasets](#datasets))

(note: not 100% sure this will work at the moment)

## Multi-task learning
(Although I wanted to call it 'hydra training', I should use the correct terminology)

You can do multi-task learning by simply calling:

```
th main.lua
```

And append 

```
-datasetNames <insert dataset names here> -outputSizes <insert corresponding sizes here>
```

For whatever datasets you have

## Faked input

Simply use the -propFake flag to set the proportion of each minibatch that consists of generated fake data.
Default is 0.25


# Set-up

It's possible to use this without docker, but the docker instance used for this project is my [friend/colleague's kinda-monolithic docker torch image](https://github.com/anibali/docker-torch)
The torch libraries used:

* nn
* cunn
* cudnn
* optim

The extra libraries used for this project are:

* Penlight
* Torchnet
* cjson

## Datasets

The data sets used are [AgNews](https://www.di.unipi.it/~gulli/AG_corpus_of_news_articles.html), [AmazonReviews](https://snap.stanford.edu/data/web-Amazon.html)  (for full dataset, you need to e-mail Julian McAuley (julian.mcauley@gmail.com)), [YahooQuestionAnswer (L6)](https://webscope.sandbox.yahoo.com/catalog.php?datatype=l) and [YelpReviews](https://www.yelp.com/dataset_challenge)

Once these have been downloaded you need to convert them to a common indexed dataset format with the torchnet libarary. The output folder locations are hard-coded and they take the folder that has the data as a command line input, eg.

```
cd ./data &
th agNews.lua /fastdata/agNews
```

will take the xml downloaded into the /fastdata/agNews folder and generate an index fild and a binary file for each the training dataset and testing dataset to a fixed location on the computer.

There are scripts in the ./data/ folder for each of the previously mentioned datasets. See ./data/strToCharTens.lua for a description of how the text is encoded as a fixed-length char tensor.

Note that the expected folder structure will be

* datasetFolder
    * train
        * <dataset1>.idx
        * <dataset1>.bin
        * <dataset2>.idx
        * <dataset2>.bin
        * ...
    * test
        * <dataset1>.idx
        * <dataset1>.bin
        * <dataset2>.idx
        * <dataset2>.bin
        * ...

## Visualising

This also uses a system for visualising made by [the same cool colleague](https://github.com/anibali/showoff).
When used with [docker-compose](https://docs.docker.com/compose/), and the showoff image made, you can use the -displayEvery flag for main.lua to indicate after how many iterations should the visualisation be updated.

There is a graph for training and validation loss, as well as an estimate of accuracy.